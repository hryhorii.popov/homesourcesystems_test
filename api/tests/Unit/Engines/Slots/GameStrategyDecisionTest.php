<?php

namespace Tests\Unit\Engines\Slots;

use App\DataObjects\Games\Slots\GameSessionDataObject;
use App\Engines\Slots\Concretes\AdjustedSpin;
use App\Engines\Slots\Concretes\HardlyAdjustedSpin;
use App\Engines\Slots\Concretes\UsualSpin;
use App\Engines\Slots\GameStrategyDecision;
use Tests\TestCase;

class GameStrategyDecisionTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function test_it_resolves_concrete_by_the_rules(): void
    {
        /** PREPARE */
        $gameSessionWithUsualSpins = GameSessionDataObject::from([
            'credits' => rand(1, 39)
        ]);
        $gameSessionWithAdjustedSpins = GameSessionDataObject::from([
            'credits' => rand(41, 60)
        ]);
        $gameSessionWithHardlyAdjustedSpins = GameSessionDataObject::from([
            'credits' => rand(61, 999)
        ]);

        /** EXECUTE */
        $usualContext = GameStrategyDecision::decide($gameSessionWithUsualSpins);
        $adjustedContext = GameStrategyDecision::decide($gameSessionWithAdjustedSpins);
        $hardlyAdjustedContext = GameStrategyDecision::decide($gameSessionWithHardlyAdjustedSpins);

        /** ASSERT */
        $this->assertTrue($usualContext instanceof UsualSpin);
        $this->assertTrue($adjustedContext instanceof AdjustedSpin);
        $this->assertTrue($hardlyAdjustedContext instanceof HardlyAdjustedSpin);
    }
}
