<?php

namespace Tests\Feature\Games\Slots;

use App\DataObjects\Games\Slots\GameSessionDataObject;
use App\Services\Infrastructure\RedisService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Tests\ApiTestCase;
use Tests\Helpers\Games\Slots\SlotGameHelper;

class SlotGameControllerTest extends ApiTestCase
{
    use SlotGameHelper;

    protected function setUp(): void
    {
        parent::setUp();
        RedisService::clearAll();
    }

    public function test_i_can_init_game_as_a_guest(): void
    {
        /** PREPARE */
        /** EXECUTE */
        $response = $this->post(route('api.v1.games.slots.init'));

        /** ASSERT */
        $response->assertCreated();
        $this->assertIsArray(Arr::get($response, 'data.attempt'));
        $this->assertNotNull(Arr::get($response, 'data.attempt.session_id'));
        $key = Arr::get($response, 'data.attempt.session_id');

        $gameSession = GameSessionDataObject::from(RedisService::get($key));
        $this->assertEquals(config('games.slots.initial_credits'), $gameSession->credits);
    }

    public function test_i_can_roll_slots(): void
    {
        /** PREPARE */
        $session = $this->setUpGameSession();

        /** EXECUTE */
        $response = $this->post(route('api.v1.games.slots.roll'), [], [
            'Game' => $session->sessionId,
        ]);

        /** ASSERT */
        $response->assertOk();
        $data = Arr::get($response, 'data');
        $this->assertArrayHasKey('attempt', $data);
        $this->assertArrayHasKey('session_id', Arr::get($data, 'attempt'));
        $this->assertArrayHasKey('result', Arr::get($data, 'attempt'));
        $this->assertArrayHasKey('session', Arr::get($data, 'attempt'));
    }

    public function test_i_cash_out_and_leave(): void
    {
        /** PREPARE */
        $session = $this->setUpGameSession();

        /** EXECUTE */
        $response = $this->post(route('api.v1.games.slots.roll'), [], [
            'GameSessionId' => $session->sessionId,
        ]);

        /** ASSERT */
        $response->assertOk();
        $data = Arr::get($response, 'data');
        $this->assertArrayHasKey('attempt', $data);
        $this->assertArrayHasKey('session_id', Arr::get($data, 'attempt'));
        $this->assertArrayHasKey('result', Arr::get($data, 'attempt'));
        $this->assertArrayHasKey('session', Arr::get($data, 'attempt'));
    }
}
