<?php

namespace tests\Helpers\Games\Slots;

use App\DataObjects\Games\Slots\GameSessionDataObject;
use App\DataObjects\Infrastructure\Services\Games\Slots\SessionInitializeDataObject;
use App\Services\Games\Slots\GameSessionService;
use App\Services\Games\Slots\SlotGameService;
use Illuminate\Support\Facades\App;

trait SlotGameHelper
{
    protected function setUpGameSession(): SessionInitializeDataObject
    {
        /** @var SlotGameService $slotGameService */
        $slotGameService = App::make(SlotGameService::class);

        return $slotGameService->initialize();
    }

    protected function setSessionAmount(
        string $sessionId,
        int    $amount
    ): GameSessionDataObject
    {
        /**
         * @var GameSessionService $gameSessionService
         */
        $gameSessionService = App::make(GameSessionService::class);
        $gameSessionService->setSessionBalance($sessionId, $amount);

        return $gameSessionService->getSession($sessionId);
    }
}
