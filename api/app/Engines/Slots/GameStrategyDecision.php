<?php

namespace App\Engines\Slots;

use App\DataObjects\Games\Slots\GameSessionDataObject;
use App\Engines\Slots\Concretes\AdjustedSpin;
use App\Engines\Slots\Concretes\HardlyAdjustedSpin;
use App\Engines\Slots\Concretes\UsualSpin;
use App\Interfaces\Engines\Slots\GameContextStrategyInterface;
use Illuminate\Support\Facades\App;

class GameStrategyDecision
{
    public static function decide(
        GameSessionDataObject $session
    ): GameContextStrategyInterface
    {
        if ($session->credits < 40) {
            return App::make(UsualSpin::class);
        }

        if ($session->credits <= 60) {
            return App::make(AdjustedSpin::class);
        }

        return App::make(HardlyAdjustedSpin::class);
    }
}
