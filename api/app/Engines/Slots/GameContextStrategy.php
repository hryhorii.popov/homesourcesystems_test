<?php

namespace App\Engines\Slots;

use App\DataObjects\Engines\Slots\GameRoundResultDataObject;
use App\Interfaces\Engines\Slots\GameContextStrategyInterface;

class GameContextStrategy
{
    public function __construct(
        private readonly GameContextStrategyInterface $strategy,
    ) {}

    public function execute(): GameRoundResultDataObject
    {
        return $this->strategy->execute();
    }
}
