<?php

namespace App\Engines\Slots\Concretes;

use App\DataObjects\Engines\Slots\GameRoundResultDataObject;
use App\Enums\Engines\Slots\SlotTypes;
use App\Interfaces\Engines\Slots\GameContextStrategyInterface;

abstract class BaseSpin implements GameContextStrategyInterface
{
    protected readonly array $options;
    protected readonly GameRoundResultDataObject $result;

    public function __construct()
    {
        $this->options = SlotTypes::values();
        $this->result = GameRoundResultDataObject::from([
            'positions' => [],
            'win' => false,
        ]);
    }

    public function execute(): GameRoundResultDataObject
    {
        $this->rollTheSlots();

        if ($this->result->win && $this->shouldReRoll()) {
            $this->rollTheSlots();
        }

        return $this->result;
    }

    protected function rollTheSlots(): void
    {
        $results = [];
        for ($i = 0; $i < 3; $i++) {
            $results[] = $this->options[array_rand($this->options)];
        }
        $this->result->positions = $results;
        $this->determinateIfIsWin($this->result);
    }

    protected function shouldReRoll(): bool
    {
        return false;
    }

    private function determinateIfIsWin(
        GameRoundResultDataObject $result
    ): void
    {
        $this->result->win = (
            count(
                array_unique(
                    $this->result->positions,
                    SORT_REGULAR
                )
            ) === 1
        );
    }
}
