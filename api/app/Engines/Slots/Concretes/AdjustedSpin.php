<?php

namespace App\Engines\Slots\Concretes;

use App\DataObjects\Engines\Slots\GameRoundResultDataObject;
use App\Enums\Engines\Slots\SlotTypes;
use App\Interfaces\Engines\Slots\GameContextStrategyInterface;

class AdjustedSpin extends BaseSpin
{
    protected function shouldReRoll(): bool
    {
        return rand(0, 100) < 30;
    }
}
