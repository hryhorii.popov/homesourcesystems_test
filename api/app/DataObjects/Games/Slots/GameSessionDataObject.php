<?php

namespace App\DataObjects\Games\Slots;

use App\DataObjects\BaseDataObject;

class GameSessionDataObject extends BaseDataObject
{
    public int $credits;
}
