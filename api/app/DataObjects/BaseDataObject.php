<?php

namespace App\DataObjects;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataCollection;

abstract class BaseDataObject extends Data
{
    public function syncPropertyValueToChild(
        string $targetPropertyName,
        mixed  $value,
        array  $ignoreFor = []
    ): void
    {
        foreach ($this->getPropertiesForSync() as $property) {
            if (isset($this->{$property})) {
                if (is_array($this->{$property}) || $this->{$property} instanceof DataCollection) {
                    $collection = $this->{$property};

                    foreach ($collection as $entry) {
                        if (
                            property_exists($entry, $targetPropertyName)
                            && $entry->{$targetPropertyName} === null
                            && !in_array($property, $ignoreFor)
                        ) {
                            $entry->{$targetPropertyName} = $value;
                        }
                    }
                } else {
                    if (
                        property_exists($this->{$property}, $targetPropertyName)
                        && $this->{$property}->{$targetPropertyName} === null
                        && !in_array($property, $ignoreFor)
                    ) {
                        $this->{$property}->{$targetPropertyName} = $value;
                    }
                }
            }
        }
    }

    protected function getPropertiesForSync(): array
    {
        return [];
    }
}
