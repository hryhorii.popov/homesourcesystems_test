<?php

namespace App\DataObjects\Engines\Slots;

use App\DataObjects\BaseDataObject;

class GameRoundResultDataObject extends BaseDataObject
{
    public array $positions;
    public bool $win;
}
