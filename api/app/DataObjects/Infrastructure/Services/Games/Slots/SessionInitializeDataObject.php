<?php

namespace App\DataObjects\Infrastructure\Services\Games\Slots;

use App\DataObjects\BaseDataObject;
use Spatie\LaravelData\Attributes\MapName;

class SessionInitializeDataObject extends BaseDataObject
{
    #[MapName('session_id')]
    public null|string $sessionId;
    public int $credits;
}
