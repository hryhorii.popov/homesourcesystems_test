<?php

namespace App\DataObjects\Infrastructure\Services\Games\Slots;

use App\DataObjects\BaseDataObject;
use App\DataObjects\Engines\Slots\GameRoundResultDataObject;
use App\DataObjects\Games\Slots\GameSessionDataObject;
use Spatie\LaravelData\Attributes\MapName;

class SpinRoundResultDataObject extends BaseDataObject
{
    #[MapName('session_id')]
    public null|string $sessionId;
    public GameRoundResultDataObject $result;
    public GameSessionDataObject $session;
}
