<?php

namespace App\Services\Games\Slots;

use App\DataObjects\Games\Slots\GameSessionDataObject;
use App\Exceptions\Services\Games\Slots\DeleteForbiddenException;
use App\Services\Infrastructure\RedisService;

class GameSessionService
{
    public function setSessionBalance(string $sessionId, int $amount): void
    {
        $gameBalance = GameSessionDataObject::from([
            'credits' => $amount,
        ]);
        RedisService::set($sessionId, $gameBalance->toJson());
    }

    public function getSession(string $sessionId): GameSessionDataObject|null
    {
        $session = RedisService::get($sessionId);

        return isset($session)
            ? GameSessionDataObject::from(RedisService::get($sessionId))
            : null;
    }

    public function delete(
        string $sessionId,
        bool   $secure = true
    ): void
    {
        $session = $this->getSession($sessionId);

        if ($session->credits === 0) {
            RedisService::delete($sessionId);
            return;
        }

        if ($secure) {
            throw new DeleteForbiddenException();
        }
    }

}
