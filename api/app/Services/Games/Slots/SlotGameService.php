<?php

namespace App\Services\Games\Slots;

use App\DataObjects\Engines\Slots\GameRoundResultDataObject;
use App\DataObjects\Infrastructure\Services\Games\Slots\SessionInitializeDataObject;
use App\DataObjects\Infrastructure\Services\Games\Slots\SpinRoundResultDataObject;
use App\Engines\Slots\GameContextStrategy;
use App\Maps\Engines\Slots\SlotOptionWeightTypes;
use Illuminate\Support\Facades\App;
use Ramsey\Uuid\Uuid;

class SlotGameService
{
    public function __construct(
        private readonly GameSessionService $gameSessionService,
    )
    {
    }

    public function initialize(): SessionInitializeDataObject
    {
        $sessionId = Uuid::uuid4()->toString();
        $this->gameSessionService->setSessionBalance($sessionId, config('games.slots.initial_credits'));

        return SessionInitializeDataObject::from([
            'sessionId' => $sessionId,
            'credits' => config('games.slots.initial_credits'),
        ]);
    }

    public function roll(string $sessionId): SpinRoundResultDataObject
    {
        /**
         * @var GameContextStrategy $gameContext
         */
        $gameSession = $this->gameSessionService->getSession($sessionId);
        $gameContext = App::make(GameContextStrategy::class, [
            'session' => $gameSession
        ]);

        $result = $gameContext->execute();
        $this->processResult($sessionId, $result);

        return SpinRoundResultDataObject::from([
            'sessionId' => $sessionId,
            'result' => $result,
            'session' => $this->gameSessionService->getSession($sessionId),
        ]);
    }

    public function cashOut(string $sessionId): bool
    {
        $this->gameSessionService->setSessionBalance($sessionId, 0);
        $this->gameSessionService->delete($sessionId);
        $session = $this->gameSessionService->getSession($sessionId);

        return !isset($session);
    }

    private function processResult(
        string                    $sessionKey,
        GameRoundResultDataObject $gameRoundResult
    ): void
    {
        $rewardMap = SlotOptionWeightTypes::getWinRates();
        $balance = $this->gameSessionService->getSession($sessionKey)->credits - 1;

        if ($gameRoundResult->win) {
            $balance = $balance + $rewardMap[$gameRoundResult->positions[0]];
        }

        $this->gameSessionService->setSessionBalance($sessionKey, $balance);
    }
}
