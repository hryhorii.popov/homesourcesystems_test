<?php

namespace App\Services\Infrastructure;

use Illuminate\Support\Facades\Redis;
use RedisException;

class RedisService
{
    /**
     * @throws RedisException
     */
    public static function get(
        string $key
    ): false|Redis|string|null
    {
        return Redis::get($key);
    }

    /**
     * @throws RedisException
     */
    public static function set(
        string $key,
        string $value
    ): bool|Redis
    {
        return Redis::set($key, $value);
    }

    /**
     * @throws RedisException
     */
    public static function delete(
        string $key,
    ): void
    {
        Redis::del($key);
    }

    public static function clearAll()
    {
        Redis::command('FLUSHALL');
    }
}
