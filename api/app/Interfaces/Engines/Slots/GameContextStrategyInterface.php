<?php

namespace App\Interfaces\Engines\Slots;

use App\DataObjects\Engines\Slots\GameRoundResultDataObject;
use App\DataObjects\Games\Slots\GameSessionDataObject;

interface GameContextStrategyInterface
{
    public function execute(): GameRoundResultDataObject;
}
