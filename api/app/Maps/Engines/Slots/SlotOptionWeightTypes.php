<?php

namespace App\Maps\Engines\Slots;

use App\Enums\Engines\Slots\SlotTypes;

class SlotOptionWeightTypes
{
    public static function getWinRates(): array
    {
        return [
            SlotTypes::C->value => 10,
            SlotTypes::L->value => 20,
            SlotTypes::O->value => 30,
            SlotTypes::W->value => 40,
        ];
    }

}
