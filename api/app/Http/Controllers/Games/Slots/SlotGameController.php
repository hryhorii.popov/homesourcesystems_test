<?php

namespace App\Http\Controllers\Games\Slots;

use App\Http\Controllers\Controller;
use App\Services\Games\Slots\SlotGameService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SlotGameController extends Controller
{
    public function __construct(
        private readonly SlotGameService $slotGameService
    )
    {
    }

    public function init(
        Request $request
    ): JsonResponse
    {
        $attempt = $this->slotGameService->initialize();

        return response()->json([
            'data' => [
                'attempt' => $attempt,
            ]
        ], Response::HTTP_CREATED);
    }

    public function roll(
        Request $request
    ): JsonResponse
    {
        $sessionId = $request->header('Game');
        $result = $this->slotGameService->roll($sessionId);

        return response()->json([
            'data' => [
                'attempt' => $result,
            ],
        ]);
    }

    public function cashOut(
        Request $request
    ): JsonResponse
    {
        $sessionId = $request->header('Game');
        $result = $this->slotGameService->cashOut($sessionId);

        return response()->json([
            'data' => [
                'attempt' => $result,
            ],
        ]);
    }
}
