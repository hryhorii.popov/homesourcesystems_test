<?php

namespace App\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Level;
use Monolog\Logger;
use Monolog\LogRecord;

class LogHandler extends AbstractProcessingHandler
{
    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  LogRecord $record
     * @return void
     */
    protected function write(LogRecord $record): void
    {
        switch ($record->level) {
            case Level::Info:
                $this->accessLogBehaviour($record);
                break;
            case Level::Error:
                $this->errorLogBehaviour($record);
                break;
            default:
                //Unknown log type => ignoring
                break;
        }
    }

    /**
     * What shall we do when error has been occurred? Slack? Email? You can set your own behaviour
     */
    private function errorLogBehaviour(LogRecord $record): bool
    {
        $storage = Storage::disk('local');

        if (!$storage->exists('custom_error_logs')) {
            $storage->makeDirectory('custom_error_logs');
        }

        $logPath = "custom_error_logs/error.log";

//        if (!$storage->exists($logPath)) {
//            return $storage->put($logPath, json_encode($record));
//        }

        //return $storage->append($logPath, json_encode($record));

        return true;
    }

    /**
     * This behaviour for the application access
     */
    private function accessLogBehaviour(LogRecord $record): bool
    {
        $storage = Storage::disk('local');

        if (!$storage->exists('custom_access_logs')) {
            $storage->makeDirectory('custom_access_logs');
        }

        $logPath = "custom_access_logs/access.log";

//        if (!$storage->exists($logPath)) {
//            return $storage->put($logPath, json_encode($record));
//        }

        //return $storage->append($logPath, json_encode($record));

        return true;
    }
}
