<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Monolog\Logger;
use Illuminate\Http\Response;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response as BaseResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * Class ExceptionRender
 */
class ExceptionRender
{
    /**
     * Handles any unspecified exception
     *
     * @param string $methodName
     * @param array $params
     *
     * @return JsonResponse
     */
    public function __call(
        string $methodName,
        array  $params
    ): JsonResponse
    {
        $exception = array_shift($params);
        $message = "";
        $code = Response::HTTP_INTERNAL_SERVER_ERROR;

        if ($exception instanceof HttpException) {
            $message = $exception->getMessage();
            $code = $exception->getStatusCode();
        } else if ($exception instanceof Exception) {
            $message = $exception->getMessage();
            $code = $exception->getCode();
        }

        return response()->json([
            'request_id' => Uuid::uuid4()->toString(),
            'data' => null,
            'errors' => [
                'messages' => $message
            ]
        ], self::isResponseCodeValid((int)$code) ?
            $code :
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

    /**
     * Exception for ValidationException
     *
     * @param ValidationException $exception
     *
     * @return JsonResponse
     */
    public function validationException(ValidationException $exception): JsonResponse
    {
        return $this->jsonResponse(
            $exception->getMessage(),
            $exception->errors(),
            Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * Exception for ModelNotFoundException
     *
     * @param Exception $exception
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function modelNotFoundException(Exception $exception, Request $request): JsonResponse
    {
        return $this->jsonResponse(
            $exception->getMessage() ?:
                __("exception.notFound", [
                        'number' => $request->route('code')]
                ),
            [],
            Response::HTTP_NOT_FOUND
        );
    }

    /**
     * Exception for ModelNotFoundException
     *
     * @param Exception $exception
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function notFoundHttpException(Exception $exception, Request $request): JsonResponse
    {
        $params = ($request->route()) ? $request->route()->parameters() : null;
        $id = is_array($params) ? array_shift($params) : null;

        return $this->jsonResponse(
            $exception->getMessage() ?: __("exception.notFound", ['number' => $id]),
            [],
            Response::HTTP_NOT_FOUND
        );
    }

    /**
     * Exception for AuthenticationException
     *
     * @param Exception $exception
     *
     * @return JsonResponse
     */
    public function authenticationException(Exception $exception): JsonResponse
    {
        return $this->jsonResponse(
            $exception->getMessage() ?: __("auth.unauthorized"),
            [],
            Response::HTTP_UNAUTHORIZED
        );
    }

    /**
     * Exception for QueryException
     *
     * @param Exception $exception
     *
     * @return JsonResponse
     */
    public function queryException(Exception $exception): JsonResponse
    {
        return $this->jsonResponse(
            $exception->getMessage() ?: __("exception.queryException"),
            [],
            Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * @param Exception $exception
     *
     * @return JsonResponse
     */
    public function invalidArgumentException(Exception $exception): JsonResponse
    {
        return $this->jsonResponse(
            $exception->getMessage() ?: __("exception.invalidArgument"),
            [],
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

    /**
     * Template for all json Response
     *
     * @param string $message
     * @param array|null $fields
     * @param int $code
     *
     * @return JsonResponse
     */
    protected function jsonResponse(
        string $message,
        array  $fields = null,
        int    $code = Response::HTTP_OK
    ): JsonResponse
    {
        return response()->json([
            'request_id' => app(Logger::class)->requestUID,
            'data' => null,
            'errors' => [
                'message' => $message,
                'fields' => $fields
            ]
        ], $code);
    }

    protected static function isResponseCodeValid(int $code): bool
    {
        $validCodes = array_keys(BaseResponse::$statusTexts);

        return in_array($code, $validCodes);
    }

}
