<?php

namespace App\Exceptions\Services\Games\Slots;

use Exception;
use Illuminate\Http\Response;
use Throwable;

class DeleteForbiddenException extends Exception
{
    /**
     * DeleteForbiddenException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string     $message  = "",
        int        $code     = Response::HTTP_BAD_REQUEST,
        ?Throwable $previous = null
    ) {
        parent::__construct(
            $message ?: 'You can not close session with positive balance',
            $code,
            $previous
        );
    }
}
