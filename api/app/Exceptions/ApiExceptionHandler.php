<?php

namespace App\Exceptions;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Str;
use Throwable;

class ApiExceptionHandler extends Handler
{
    /**
     * @var ExceptionRender $exceptionRender
     */
    protected ExceptionRender $exceptionRender;

    /**
     * ApiExceptionHandler constructor.
     *
     * @param Container       $container
     * @param ExceptionRender $exceptionRender
     */
    public function __construct(
        Container $container,
        ExceptionRender $exceptionRender
    ) {
        parent::__construct($container);

        $this->exceptionRender = $exceptionRender;
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Throwable                $exception
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Throwable $exception)
    {
        $methodName = Str::camel(class_basename($exception));

        return $this->exceptionRender->{$methodName}($exception, $request);
    }
}
