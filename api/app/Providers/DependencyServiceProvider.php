<?php

namespace App\Providers;

use App\Engines\Slots\GameContextStrategy;
use App\Engines\Slots\GameStrategyDecision;
use App\Exceptions\ApiExceptionHandler;
use App\Helpers\LogHandler;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Application;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

class DependencyServiceProvider extends ServiceProvider
{
    public function bind()
    {
    }

    public function register()
    {
        $this->app->singleton(GameContextStrategy::class, function (
            Container $app,
            array     $config
        ) {
            return GameStrategyDecision::decide(Arr::get($config, 'session'));
        });

        $this->app->bind(ExceptionHandler::class, ApiExceptionHandler::class);

        $this->app->singleton(
            Logger::class,
            function (Application $app) {
                $logger = new Logger('single');
                $logger->pushHandler(new LogHandler());
                $logger->requestUID = $requestUID = (new UidProcessor(16))->getUid();

                /**
                 * Callable will be called each time the record will be generated (for the log entry).
                 * Here we set unique ID for each request.
                 * This UID will be constantly declared for the whole request lifecycle
                 * Additionally we replace secure data like password and token.
                 */
                $logger->pushProcessor(
                    function ($record) use ($requestUID) {

                        $message = json_decode(Arr::get($record, 'message', "{}"), true);

                        if (isset($message['params']['password'])) {
                            Arr::set($message, 'params.password', '******');
                        }

                        if (isset($message['headers']['authorization'])) {
                            Arr::set($message, 'headers.authorization', '******');
                        }

                        Arr::set($record, 'id', $requestUID);
                        Arr::set($record, 'message', json_encode($message));

                        return $record;
                    }
                );
                return $logger;
            }
        );
    }
}
