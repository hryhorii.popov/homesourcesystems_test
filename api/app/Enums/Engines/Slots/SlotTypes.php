<?php

namespace App\Enums\Engines\Slots;

use ArchTech\Enums\Values;

enum SlotTypes: string
{
    use Values;

    case C = 'c';
    case L = 'l';
    case O = 'o';
    case W = 'w';
}
