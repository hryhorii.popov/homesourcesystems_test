<?php

use App\Http\Controllers\Games\Slots\SlotGameController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::group([
    'prefix' => '/v1',
    'as' => 'v1.',
    'namespace' => 'v1',
], function () {
    Route::get('/ping', function () {
        return 'pong :)';
    })->name('test');


    Route::group([
        'prefix' => '/games',
        'as' => 'games.',
    ], function () {
        Route::group([
            'prefix' => '/slots',
            'as' => 'slots.',
        ], function () {
            Route::post('/init', [SlotGameController::class, 'init',])->name('init');
            Route::post('/roll', [SlotGameController::class, 'roll',])->name('roll');
            Route::post('/cash-out', [SlotGameController::class, 'cashOut',])->name('cash_out');
        });
    });
});
