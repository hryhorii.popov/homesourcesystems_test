function deepFreeze(obj) {
  if (Array.isArray(obj)) {
  // only freeze the array itself, allow modifying of the options objects inside of it
  // since rules like to add default options to the provided objects
  // for (const elem of obj) {
  //     deepFreeze(elem);
  // }

    Object.freeze(obj);
  } else if (obj !== undefined && obj !== null && typeof obj === "object") {
    for (const k of Object.keys(obj)) {
      deepFreeze(obj[k]);
    }

    Object.freeze(obj);
  }

  return obj;
}

// deep freeze is needed to prevent some package eslintrc from importing this object and messing with it
// (like changing warn to error, but not as override in their package, but in this config)
module.exports = deepFreeze({
  root: true,
  extends: [
    "airbnb",
    "airbnb-typescript",
    "airbnb/hooks",
  ],
  plugins: [
    "import",
    "import-newlines",
    "@typescript-eslint/eslint-plugin",
    "typescript-sort-keys",
    "react",
    "react-hooks",
    "jest",
    "eslint-comments",
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2017,
    sourceType: "module",
  },
  ignorePatterns: [
    // careful to not add root-relative paths, since when importiung these in
    // package .eslintrc files these are treated relative to that folder.
    "**/node_modules/",
    "**/lib/",
    "!**/.storybook/",
    "*.config.js",
    "*.svg",
    "*.scss",
    ".eslintrc.js",
  ],
  rules: {
    // ****************** eslint ******************
    // https://eslint.org/docs/rules/
    "react/no-unstable-nested-components": "off",
    "react/jsx-no-useless-fragment": "off",
    "react/jsx-no-bind": "warn",
    "arrow-body-style": "off",
    "arrow-parens": "off",
    "class-methods-use-this": "off",
    "consistent-return": "off",
    "curly": ["error", "all"], // stricter than airbnb has
    "default-case": "off",
    /**
     * NOTE: there are some issues in TS (when a generic function)
     * TODO: migrate to TS when it's implemented
     * @see https://github.com/typescript-eslint/typescript-eslint/issues/3318
     * @see https://github.com/typescript-eslint/typescript-eslint/issues/942
     * TODO: resolve all issues and remove the rule because it exists in airbnb
     * or
     * TODO: to try doing with prettier
     * should be ["error", "multiline-arguments"]
     */
    "function-paren-newline": "off",
// TODO: resolve all issues and remove the rule because it exists in airbnb
    "global-require": "warn",
    "implicit-arrow-linebreak": "off",
    "max-classes-per-file": "off",
    "max-len": ["error", {
      code: 140,
      comments: 0,
      ignorePattern: `^import |/.+eslint-.+able|//|"[^"]{100,}"`,
      "ignoreStrings": true,
    }],
    "newline-per-chained-call": "off",
    "no-await-in-loop": "off",
    "no-case-declarations": "off",
    "no-confusing-arrow": "off",
    "no-console": "error",
    "no-continue": "off",
    "no-restricted-imports": ["warn", "enzyme"],
    "no-empty": ["error", { allowEmptyCatch: true }],
    "no-inner-declarations": "off", // not actual since we target only ES6
    "no-irregular-whitespace": ["error", { // stricter than airbnb has
      skipStrings: false,
      skipComments: false,
      skipRegExps: false,
      skipTemplates: false,
    }],
    "no-mixed-operators": ["error", {
      groups: [
        ["??", "==", "!=", "===", "!=="], // block `a ?? b === c` and similar
      ],
    }],
    "no-nested-ternary": "off",
// TODO: resolve all issues and switch to "error"
    "no-param-reassign": ["warn", { props: false }],
    "no-plusplus": "off",
    "no-prototype-builtins": "warn",
    "no-restricted-properties": ["error",
      {
        object: "process",
        property: "env",
        message: "Use strongly typed BUILD_OPTIONS instead. See MOBREF-1628",
      },
      {
        object: "arguments",
        property: "callee",
        message: "arguments.callee is deprecated",
      },
    ],
    "no-restricted-syntax": [
      "error",
      {
        selector: "LabeledStatement",
        message: "Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.",
      },
      {
        selector: "WithStatement",
        message: "`with` is disallowed in strict mode because it makes code impossible to predict and optimize.",
      },
    ],
    "no-underscore-dangle": "off",
    "no-useless-escape": "warn", // there are places where escape make sense even if not strictly required. For example `/[[\]]/` vs `/[\[\]]/`
    "object-curly-newline": ["error", {
      ObjectExpression: { multiline: true, consistent: true },
      ObjectPattern: { multiline: true, consistent: true },
      ImportDeclaration: { multiline: true, consistent: true },
      ExportDeclaration: { multiline: true, consistent: true },
    }],
    "object-shorthand": "off",
    "operator-linebreak": "off",
    "prefer-arrow-callback": ["error", {
      allowNamedFunctions: true,
      allowUnboundThis: true,
    }],
    "prefer-destructuring": "off",
// NOTE: if we want to switch to "error", just remove the rule because it exists in airbnb
    "prefer-exponentiation-operator": "warn",
    "prefer-promise-reject-errors": ["warn", { allowEmptyReject: true }],
    "prefer-spread": "off",
    "prefer-template": "warn",
    "quote-props": ["error", "consistent-as-needed"],

// ****************** @typescript-eslint ******************
// https://typescript-eslint.io/rules/

    "@typescript-eslint/adjacent-overload-signatures": "error",
// TODO: resolve all issues and switch to "error"
    "@typescript-eslint/ban-ts-comment": ["warn", {
      "ts-expect-error": "allow-with-description",
      "ts-ignore": "allow-with-description",
      "ts-nocheck": "allow-with-description",
      "ts-check": false,
      "minimumDescriptionLength": 3,
    }],
    "@typescript-eslint/ban-tslint-comment": "error",
    "@typescript-eslint/ban-types": "error",
    "@typescript-eslint/consistent-indexed-object-style": ["error", "record"],
    "@typescript-eslint/consistent-type-imports": [
      "error",
      {
        prefer: "type-imports",
        disallowTypeAnnotations: false,
        fixStyle: "inline-type-imports",
      },
    ],
    "@typescript-eslint/no-import-type-side-effects": "error",
    "@typescript-eslint/dot-notation": "off",
    "@typescript-eslint/explicit-member-accessibility": ["error", { accessibility: "explicit" }],
    "@typescript-eslint/explicit-module-boundary-types": ["warn", {
      allowArgumentsExplicitlyTypedAsAny: false,
      allowDirectConstAssertionInArrowFunctions: true,
      allowedNames: [
// ignore React lifecycle methods
        "render",
        "getInitialState",
        "getDefaultProps",
        "getChildContext",
        "componentWillMount",
        "UNSAFE_componentWillMount",
        "componentDidMount",
        "componentWillReceiveProps",
        "UNSAFE_componentWillReceiveProps",
        "shouldComponentUpdate",
        "componentWillUpdate",
        "UNSAFE_componentWillUpdate",
        "componentDidUpdate",
        "componentWillUnmount",
        "componentDidCatch",
        "getSnapshotBeforeUpdate",
      ],
      allowHigherOrderFunctions: true,
      allowTypedFunctionExpressions: true,
    }],
    "@typescript-eslint/indent": ["error", 2, {
      SwitchCase: 1,
      VariableDeclarator: 1,
      outerIIFEBody: 1,
      MemberExpression: 1,
      FunctionDeclaration: {
        parameters: 1,
        body: 1,
      },
      FunctionExpression: {
        parameters: 1,
        body: 1,
      },
      CallExpression: {
        arguments: 1,
      },
      ArrayExpression: 1,
      ObjectExpression: 1,
      ImportDeclaration: 1,
      flatTernaryExpressions: false,
      ignoredNodes: [
// airbnb list
        "JSXElement", "JSXElement > *", "JSXAttribute", "JSXIdentifier", "JSXNamespacedName",
        "JSXMemberExpression", "JSXSpreadAttribute", "JSXExpressionContainer", "JSXOpeningElement",
        "JSXClosingElement", "JSXFragment", "JSXOpeningFragment", "JSXClosingFragment", "JSXText",
        "JSXEmptyExpression", "JSXSpreadChild",

// additional custom nodes

        /**
         * TS AST nodes (nodes names can differ depending on TS parser).
         * @see https://astexplorer.net/
         *
         * NOTE: Need to ignore some TS nodes until some parser issues are resolved.
         * @see https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/indent.md
         * @see https://github.com/typescript-eslint/typescript-eslint/issues/1824
         */

        /**
         * Remove when the multiline code inside Template Literals will be parsed correctly
         */
        "TemplateLiteral ConditionalExpression",
        "TemplateLiteral CallExpression",
        "TemplateLiteral TemplateElement",

        /**
         * Remove when the following code will be recognized correctly:
         * ```
         * type TooltipType = Pick<AbsTooltipProps,
         *     | "left"      // NOTE: suggests incorrect indent here
         *     | "top"       // NOTE: suggests incorrect indent here
         * >;
         * ```
         */
        "TSTypeParameterInstantiation",

        /**
         * Remove when the following code will be recognized correctly:
         * ```
         * jest.mock<typeof import("../iOSBodyClassDetection")>(
         *     "../iOSBodyClassDetection",
         *     () => {},                   // NOTE: suggests incorrect indent here
         * );
         * ```
         */
        "ExpressionStatement CallExpression ArrowFunctionExpression",

        /**
         * Remove when the following code will be recognized correctly:
         * ```
         * interface ResultHoldemProps {
         *     gameRound:
         *         | GameRoundPokerHoldem  // NOTE: suggests incorrect indent here
         *         | GameRoundPokerUth;    // NOTE: suggests incorrect indent here
         * }
         *
         * type RouletteTuple = {
         *     gameType:
         *         & GameType.RNGRoulette        // NOTE: suggests incorrect indent here
         *         & GameType.AmericanRoulette;  // NOTE: suggests incorrect indent here
         * };
         *
         * function selectTutorialShownTimes(
         *     settings: Settings,
         *     state:
         *         & Pick<RouletteState, "preferences">  // NOTE: suggests incorrect indent here
         *         & { isReady: boolean; },              // NOTE: suggests incorrect indent here
         * ): void {}
         * ```
         */
        "TSPropertySignature TSTypeAnnotation", "Identifier TSTypeAnnotation",

        /**
         * Remove when the following code will be recognized correctly:
         * ```
         * class TooltipProcessor {
         *     @action
         *     public setPickerTooltipIsAllowed = (value: boolean) => {
         *         this.isPickerTooltipAllowed = value;  // NOTE: suggests incorrect indent here
         *     };                                        // NOTE: suggests incorrect indent here
         * }
         * ```
         */
        "ClassBody.body > PropertyDefinition[decorators.length > 0] > .key",
      ],
      ignoreComments: false,
    }],
// TODO: resolve all issues and change to `allowTemplateLiterals: false`
    "@typescript-eslint/quotes": ["error", "double", { allowTemplateLiterals: true, avoidEscape: true }],
    /** In case of `exceptAfterSingleLine: false`, remove the rule because it exists in airbnb */
    "@typescript-eslint/lines-between-class-members": ["error", "always", { exceptAfterSingleLine: true }],
    "@typescript-eslint/member-delimiter-style": ["error", {
      multiline: {
        requireLast: true,
        delimiter: "semi",
      },
      singleline: {
        requireLast: true,
        delimiter: "semi",
      },
    }],
    "@typescript-eslint/member-ordering": ["error", {
      default: [
        "public-static-field",
        "public-static-method",

        "protected-static-field",
        "protected-static-method",

        "private-static-field",
        "private-static-method",

        "public-abstract-field",
        "protected-abstract-field",

        "public-instance-field",
        "protected-instance-field",
        "private-instance-field",

        "constructor",

        "public-abstract-method",
        "protected-abstract-method",

        "public-instance-method",
        "protected-instance-method",
        "private-instance-method",

        "method",
      ],
      interfaces: "never",
      typeLiterals: "never",
    }],
    "@typescript-eslint/no-empty-interface": "warn",
    "@typescript-eslint/no-explicit-any": ["warn"],
    "@typescript-eslint/no-extra-non-null-assertion": "error",
    "@typescript-eslint/no-for-in-array": "off",
    "@typescript-eslint/no-implied-eval": "off",
    "@typescript-eslint/no-inferrable-types": "error",
    "@typescript-eslint/no-loop-func": "off",
    "@typescript-eslint/no-misused-new": "error",
    "@typescript-eslint/no-namespace": ["error"],
    /**
     * TODO: resolve all issues and switch to "error"
     *
     * NOTE: There are some unclear and controversial issues for TS
     * @see https://github.com/typescript-eslint/typescript-eslint/issues/325
     */
    "@typescript-eslint/no-shadow": ["warn", {
      ignoreTypeValueShadow: false,
      ignoreFunctionTypeParameterNameValueShadow: true,
    }],
    "@typescript-eslint/no-this-alias": ["error"],
    "@typescript-eslint/no-throw-literal": "off",
    "@typescript-eslint/no-unused-expressions": ["error", {
      allowShortCircuit: false,
      allowTernary: true,
      allowTaggedTemplates: false,
      enforceForJSX: true, // this check is absent in airbnb
    }],
    "@typescript-eslint/no-use-before-define": "off",
// TODO: resolve all issues and switch to "error"
    "@typescript-eslint/no-var-requires": "warn",
// TODO: resolve all issues and switch to "error"
    "@typescript-eslint/no-non-null-asserted-optional-chain": "warn",
// TODO: resolve all issues and switch to "error"
    "@typescript-eslint/no-non-null-assertion": "warn",
    "@typescript-eslint/prefer-as-const": "error",
    "@typescript-eslint/prefer-for-of": ["error"],
    "@typescript-eslint/prefer-namespace-keyword": "error",
    "@typescript-eslint/prefer-optional-chain": "warn",
    "@typescript-eslint/return-await": "off",
    "@typescript-eslint/triple-slash-reference": ["error", { path: "never", types: "never", lib: "never" }],
    "@typescript-eslint/type-annotation-spacing": ["error"],

// ****************** import ******************
// https://github.com/import-js/eslint-plugin-import

    "import/no-default-export": "warn",
    "import/no-dynamic-require": "warn",
    "import/no-extraneous-dependencies": "off",
    // "import/no-unassigned-import": ["error", { allow: ["react", "**/*.pcss"] }],
    "import/no-unassigned-import": "off",
    "import/no-cycle": "off", // webpack has circular-dependency-plugin. import/no-cycle is very very expensive rule (when enabled, 20% of the whole lint run just for this one rule)

    /**
     * TODO: consider webpack configuration instead (!raw-loader!)
     * @see https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-webpack-loader-syntax.md
     * TODO: resolve all issues and remove the rule because it exists in airbnb or remove the comment
     */
    "import/no-webpack-loader-syntax": "off",
    "import/order": "error",
    "import/prefer-default-export": "off",
    "import-newlines/enforce": [
      "error",
      {
        "items": Infinity,
        "max-len": 120,
        "semi": true,
        "forceSingleLine": false,
      },
    ],

  // ****************** react ******************
  // https://github.com/yannickcr/eslint-plugin-react
    "react/forbid-dom-props": ["error", {
      forbid: [
        {
          propName: "data-testid",
          message: "Avoid using data-testid, it leads to unnecessary attribute duplications in DOM and testid is exposed in production, instead consider data-role. RTL is already configured to pick it when getByTestId() is used.",
        },
      ],
    }],
    "react/button-has-type": "off",
    "react/default-props-match-prop-types": "off",
// TODO: resolve all issues and remove the rule because it exists in airbnb
    "react/destructuring-assignment": ["warn", "always"],
    "react/forbid-prop-types": "off",
    "react/function-component-definition": "off",
    "react/jsx-boolean-value": ["error", "always"],
    /**
     * Could be a useful rule but requires a lot of changes in current code style, especially, for a ternary operator.
     * Perhaps, somewhen, resolve all issues and remove the rule because it exists in airbnb
     */
    "react/jsx-curly-newline": "off",
    "react/jsx-curly-spacing": ["error", { when: "never", children: true }], // no children check in airbnb
    "react/jsx-indent": ["error", 4, { checkAttributes: true, indentLogicalExpressions: true }],
    "react/jsx-indent-props": ["error", 4],
    // "react/jsx-no-bind": ["error", {
    //   ignoreRefs: false,
    //   allowArrowFunctions: false,
    //   allowFunctions: false,
    //   allowBind: false,
    //   ignoreDOMComponents: true,
    // }],
// TODO: resolve all issues and remove the rule because it exists in airbnb
    "react/jsx-no-constructed-context-values": "warn",
    "react/jsx-one-expression-per-line": "off",
    "react/jsx-props-no-spreading": "off",
// TODO: resolve all issues and remove the rule because it exists in airbnb
    "react/no-access-state-in-setstate": "warn",
// TODO: resolve all issues and remove the rule because it exists in airbnb
    "react/no-array-index-key": "warn",
    "react/no-children-prop": "warn",
// TODO: resolve all issues and remove the rule because it exists in airbnb
    "react/no-did-update-set-state": "warn",
// TODO: resolve all issues and remove the rule because it exists in airbnb
    "react/no-unused-state": "warn",
    "react/prefer-stateless-function": ["error", { ignorePureComponents: false }], // stricter than airbnb has
    "react/prop-types": "off",
    "react/react-in-jsx-scope": "off",
    "react/require-default-props": "off",
    /**
     * Could be a useful rule but requires a lot of changes.
     * Maybe worth to switch to "warn".
     * TODO: resolve all issues and remove the rule because it exists in airbnb
     */
    "react/sort-comp": "off",
    "react/state-in-constructor": "off",
    "react/static-property-placement": "off",

// ****************** react-hooks ******************
// https://reactjs.org/docs/hooks-rules.html

// TODO: resolve all issues and remove the rule because it exists in airbnb
    "react-hooks/exhaustive-deps": "warn",

// ****************** jsx-a11y ******************
// https://github.com/jsx-eslint/eslint-plugin-jsx-a11y
// at the moment we don't really need to care about accessibility

    "jsx-a11y/alt-text": "off",
    "jsx-a11y/anchor-is-valid": "off",
    "jsx-a11y/aria-role": "off",
    "jsx-a11y/click-events-have-key-events": "off",
    "jsx-a11y/control-has-associated-label": "off",
    "jsx-a11y/iframe-has-title": "off",
    "jsx-a11y/img-redundant-alt": "off",
    "jsx-a11y/interactive-supports-focus": "off",
    "jsx-a11y/label-has-associated-control": "off",
    "jsx-a11y/media-has-caption": "off",
    "jsx-a11y/mouse-events-have-key-events": "off",
    "jsx-a11y/no-autofocus": "off",
    "jsx-a11y/no-noninteractive-element-interactions": "off",
    "jsx-a11y/no-redundant-roles": "off",
    "jsx-a11y/no-static-element-interactions": "off",
  },
  overrides: [
    {
      files: ["*.d.ts"],
      rules: {
        "@typescript-eslint/triple-slash-reference": "off",
      },
    },
    {
      files: ["*.pcss.d.ts"],
      rules: {
        "import/no-default-export": "off",
      },
    },
    {
      files: ["**/scripts/*.*"],
      rules: {
        "global-require": "warn",
      },
    },
    {
      files: ["*.js"],
      rules: {
        "global-require": "off",
        "strict": "off",
// some TS rules can only be used in TypeScript files
        "@typescript-eslint/no-var-requires": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/explicit-member-accessibility": "off",
      },
    },
    {
// ignore some rules in tests files
      files: ["*.spec.*"],
      rules: {
        // warnings instead of errors
        "global-require": "warn",
        // turned off
        "@typescript-eslint/no-non-null-assertion": "off",
        "@typescript-eslint/no-use-before-define": "off",
        "import/first": "off",
        "react/jsx-no-bind": "off",
        "react/jsx-no-constructed-context-values": "off",
      },
    },
    {
      /*
       * NOTE: Jest specific rules
       * Ones selected in square brackets [] can be easily fixed
       * Ones NOT selected in [] are for team to decide or fix
       */
      files: ["*.{spec,test}.{ts,tsx}"],
      rules: {
// No imports from other *.spec files
        "no-restricted-imports": ["error", { patterns: ["*.spec"] }],
// NOTE: Enforce it() over test()
// "jest/consistent-test-it": ["warn", { fn: "it" }],
        "jest/consistent-test-it": "off",
        /*
         * NOTE: Have expect in a test body
         * In some parts of the code expect defined as another function.
         * (we can define our own assert functions is assertFunctionNames)
         */
        "jest/expect-expect": ["off", { assertFunctionNames: ["expect"] }],
        "jest/max-expects": "off",
        "jest/max-nested-describe": "off",
        "jest/no-alias-methods": "off",
// NOTE: Tests shouldn't be commented out
        "jest/no-commented-out-tests": ["off"],
        "jest/no-conditional-expect": "off",
        "jest/no-conditional-in-test": "off",
// NOTE: For safety (TDB)
        "jest/no-deprecated-functions": ["warn"],
        "jest/no-done-callback": "off",
// NOTE: Tests shouldn't have two beforeAll, afterAll hooks
        "jest/no-duplicate-hooks": ["warn"],
// NOTE: Tests Shouldn't have export const's or fn's in them
        "jest/no-export": ["off"],
        "jest/no-hooks": "off",
// NOTE: Tests shouldn't have two or more of the same title
        "jest/no-identical-title": ["warn"],
        "jest/no-interpolation-in-snapshots": "off",
        "jest/no-jasmine-globals": "error",
        "jest/no-large-snapshots": "off",
        "jest/no-mocks-import": "off",
        "jest/no-restricted-matchers": "off",
// NOTE: expect should be within the test body
        "jest/no-standalone-expect": ["warn"],
// NOTE: Enforce using .only and .skip over fit xit to improve readability
        "jest/no-test-prefixes": ["off"],
// NOTE: Tests in Jest should be void and not return values
        "jest/no-test-return-statement": ["off"],
// NOTE: Any "prefer"'s, I believe, are up to the team, but would be great if its universal everywhere
        "jest/prefer-called-with": "off",
        "jest/prefer-comparison-matcher": ["off"],
        "jest/prefer-each": ["off"],
// NOTE: prefer `expect(x).toBe(5)` instead of `expect(x === 5).toBe(true)`
        "jest/prefer-equality-matcher": ["warn"],
        "jest/prefer-expect-assertions": "off",
        "jest/prefer-expect-resolves": "off",
        "jest/prefer-hooks-in-order": ["off"],
        "jest/prefer-hooks-on-top": ["warn"],
        "jest/prefer-lowercase-title": ["off", { ignore: ["describe"] }],
        "jest/prefer-mock-promise-shorthand": "off",
        "jest/prefer-snapshot-hint": "off",
        "jest/prefer-spy-on": "off",
// NOTE: Prefer to strictly compare object's structure along with its properties
        "jest/prefer-strict-equal": ["off"],
        "jest/prefer-to-be": "off",
        "jest/prefer-to-contain": ["off"],
        "jest/prefer-to-have-length": ["warn"],
        "jest/prefer-todo": ["off"],
        "jest/require-hook": "off",
        "jest/require-to-throw-message": "off",
        "jest/require-top-level-describe": "off",
// NOTE: Describe should not be async, have params, or a return statement
        "jest/valid-describe-callback": ["off"],
// NOTE: expect() should have fixed amount of arguments and have an "expectation" made
        "jest/valid-expect": ["off"],
        "jest/valid-expect-in-promise": "off",
// NOTE: tests should start with it("should...
// NOTE: tests title should not be empty (ignoring type since a lot of titles, use enums)
        "jest/valid-title": ["off", { mustMatch: { it: "^should" }, ignoreTypeOfDescribeName: true }],
        "jest/no-disabled-tests": "error",
        "jest/no-focused-tests": "error",

// extra check to prevent focused test to be commited by mistake
        "eslint-comments/no-unlimited-disable": "error",
        "eslint-comments/no-restricted-disable": [
          "error",
          "jest/no-focused-tests",
        ],
      },
    },
    {
// ignore some rules in storybook files
      files: ["*.{story,stories}.*"],
      rules: {
        "import/no-default-export": "off",
        "react/jsx-no-bind": "off",
      },
    },
    {
      files: [
        "*.d.ts",
      ],
      rules: {
        "max-len": "off",
      },
    },
  ],
});
