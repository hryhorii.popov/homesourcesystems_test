import React from "react";

import { SlotItem } from "./slot-item.component";
import type { SlotsGameProps, SlotSymbol } from "../interfaces/slot.interface";

export const SlotsTable: React.FC<SlotsGameProps> = ({ symbols }) => {
  return (
      <div className="relative">
          <div className="flex text-center justify-center space-x-5">
              {symbols.map((symbol: SlotSymbol, index: number) => (
                  <SlotItem key={index} symbol={symbol} />
              ))}
          </div>
      </div>
  );
};
