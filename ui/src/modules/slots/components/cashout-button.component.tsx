import React, { useCallback, useRef, useState } from "react";
import { ALL_DIRECTIONS, DIR_ACTION_MAP, REVERSE_DIRECTION_MAP } from "../../../shared/constants/cashout-button-utils";

export const CashOutButton = ({ cashOut, initialized }: { cashOut: () => void; initialized: boolean; }) => {
  const [isMouseEnter, setIsMouseEnter] = useState(false);
  const buttonRef = useRef<HTMLButtonElement | null>(null);
  const [availableButtonMoveDirection, setAvailableButtonMoveDirection] = useState(ALL_DIRECTIONS);
  const [currentTransformState, setCurrentTransformState] = useState<{ x: number; y: number; }>({ x: 0, y: 0 });
  const [isDisabled, setIsDisabled] = useState<boolean>(false);

  const onMouseEnter = () => {
    if (!isMouseEnter) {
      setIsMouseEnter(true);
      handleHover();
    }
  };
  const onMouseLeave = () => {
    setIsMouseEnter(false);
    setIsDisabled(false);
  };

  const handleHover = () => {
    // @ts-ignore
    const randArray = [...Array(10).keys()];
    const randResult = randArray[Math.floor(Math.random() * randArray.length)];
    if (randResult > 8) {
      // performs cashOut();
    } else if (randResult > 5) {
      // disable
      setIsDisabled(true);
    } else {
      performJump();
    }
  };

  const performJump = useCallback(() => {
    if (!buttonRef.current) {
      return;
    }

    const directionToMove = availableButtonMoveDirection[Math.floor(Math.random() * availableButtonMoveDirection.length)];
    const action = DIR_ACTION_MAP[directionToMove];
    const transformState = currentTransformState;
    transformState.x += action.x;
    transformState.y += action.y;

    if (availableButtonMoveDirection.includes(REVERSE_DIRECTION_MAP[directionToMove])) {
      setAvailableButtonMoveDirection(availableButtonMoveDirection.filter(dir => dir !== directionToMove));
    } else {
      setAvailableButtonMoveDirection([...availableButtonMoveDirection, REVERSE_DIRECTION_MAP[directionToMove]]);
    }
    setCurrentTransformState(transformState);
  }, [isMouseEnter, availableButtonMoveDirection]);

  return (
      <button
          ref={buttonRef}
          disabled={isDisabled && initialized}
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          type="button"
          style={{ transform: `translateX(${currentTransformState.x}px) translateY(${currentTransformState.y}px)` }}
          className={`relative -ml-px inline-flex items-center rounded-r-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 ${isDisabled ? "cursor-not-allowed bg-gray-300" : "cursor-pointer hover:bg-gray-50"} focus:z-10`}
          onClick={cashOut}
      >
          Cash out
      </button>
  );
};
