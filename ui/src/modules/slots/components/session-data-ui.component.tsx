import React from "react";
import { SpinState } from "../interfaces/slot.interface";
import { CashOutButton } from "./cashout-button.component";
import { classNames } from "../../../shared/helpers/generic";

interface SessionDataUiProps {
  startGame: () => void;
  cashOut: () => void;
  slotMachineState: SpinState;
  initialized: boolean;
  credits: number;
}

export const SessionDataUi: React.FC<SessionDataUiProps> = ({
  startGame,
  cashOut,
  slotMachineState,
  initialized,
  credits,
}) => {
  return (
      <div className="isolate inline-flex rounded-md items-center justify-center w-full mt-20">
          <div className="relative inline-flex items-center rounded-l-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 bg-gray-300 focus:z-10">
              <span>$</span>
              <span>{credits}</span>
          </div>
          <div className="h-9 w-[2px] bg-white" />
          <div className="relative -ml-px inline-flex items-center bg-white rounded-r-md px-3 py-2 text-sm font-semibold text-gray-900 bg-gray-300 ring-gray-300 border-r-0 focus:z-10">
              <span className="text-gray-500">State: &nbsp;</span>
              <span>{slotMachineState}</span>
          </div>
          <div className="h-9 w-[1px] mx-4 bg-gray-300" />
          <button
              type="button"
              className={classNames(
                slotMachineState === SpinState.RUNNING
                  ? "cursor-not-allowed"
                  : "cursor-pointer",
                "relative inline-flex items-center rounded-l-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 hover:bg-gray-50 focus:z-10",
              )}
              onClick={startGame}
              disabled={slotMachineState === SpinState.RUNNING}
          >
              Start
          </button>
          <CashOutButton
              initialized={initialized}
              cashOut={cashOut}
          />
      </div>
  );
};
