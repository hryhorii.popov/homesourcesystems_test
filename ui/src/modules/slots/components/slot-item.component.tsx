import React, { useEffect, useState } from "react";

import { type Sign, type SlotSymbol, SpinState } from "../interfaces/slot.interface";
import { SYMBOL_STRING_EMOJI_MAP } from "../../../shared/constants/game-constants";

export const SlotItem: React.FC<{ symbol: SlotSymbol; }> = ({ symbol }) => {
  const [currentSymbol, setCurrentSymbol] = useState<Sign | null>(symbol.value);
  const [intervalId, setIntervalId] = useState<number | null>();

  useEffect(() => {
    if (symbol.state === SpinState.RUNNING && !intervalId) {
      const currentIntervalId = startSpin();
      setIntervalId(currentIntervalId);
    } else if (intervalId && symbol.state === SpinState.STOPPED) {
      clearInterval(intervalId);
      setIntervalId(null);
      setCurrentSymbol(symbol.value);
    }
  }, [symbol, intervalId]);

  const startSpin = (): number => {
    const symbols = Object.keys(SYMBOL_STRING_EMOJI_MAP);
    return setInterval(() => {
      setCurrentSymbol(symbols[Math.floor(Math.random() * symbols.length)] as Sign);
    }, 150) as unknown as number;
  };

  return (
      <div className="border rounded-md md:p-12 sm:p-8 p-4 text-4xl">
          <div className={`min-w-[3rem] min-h-[3rem] ${symbol.state === SpinState.RUNNING && "filter blur-sm"}`}>
              {SYMBOL_STRING_EMOJI_MAP[currentSymbol as string]}
          </div>
      </div>
  );
};
