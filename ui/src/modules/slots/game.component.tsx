import React, { useEffect, useState } from "react";
import { SlotsTable } from "./components/slots-table.component";
import { type SlotSymbol, SpinState } from "./interfaces/slot.interface";
import { SessionDataUi } from "./components/session-data-ui.component";
import {
  DEFAULT_INITIAL_SLOT_STATE,
  DEFAULT_RUNNING_SLOT_STATE,
} from "../../shared/constants/slot-state-mocks";
import { GameSlotsService } from "./services/game-slots.service";
import LoadingSpinner from "../../shared/components/loading-spinner.component";

export const Game: React.FC = () => {
  const [signs, setSigns] = useState<SlotSymbol[]>(DEFAULT_INITIAL_SLOT_STATE);
  const [credits, setCredits] = useState(0);
  const [initialLoad, setInitialLoad] = useState(true);
  const [slotMachineState, setSlotMachineState] = useState<SpinState>(SpinState.STOPPED);
  const [outOfTheGame, setOutOfTheGame] = useState(false);
  const [initialized, setInitialized] = useState(false);
  const [failed, setFailed] = useState(false);

  useEffect(() => {
    GameSlotsService.initiate().then((data: any) => {
      setCredits(data.data.attempt.credits);
      setInitialLoad(false);
      setInitialized(true);
      localStorage.setItem("game_session_id", data.data.attempt.session_id);
    }).catch(() => {
      alert("Something went wrong, please, try again later");
    });
  }, []);

  const startGame = async () => {
    setSlotMachineState(SpinState.RUNNING);
    setSigns(DEFAULT_RUNNING_SLOT_STATE);

    if (credits === 0) {
      setFailed(true);
      return;
    }

    GameSlotsService.roll()
      .then((data: any) => {
        const attempt = data.data.attempt;
        const slots = attempt.result.positions
          .map((symbol: SlotSymbol) => ({
            value: symbol,
            state: SpinState.STOPPED,
          }));
        setCredits(attempt.session.credits);

        setTimeout(() => {
          setSignsResult(slots);
        }, 1000);
      });
  };

  const cashOut = () => {
    GameSlotsService.cashOut().then(() => {
      setOutOfTheGame(true);
    });
  };

  const setSignsResult = (config: SlotSymbol[]) => {
    const SYMBOLS_COUNT = 3;

    for (let i = 0; i < SYMBOLS_COUNT; i++) {
      setTimeout(() => {
        const symbolsState = config.slice(0, i + 1).concat(DEFAULT_RUNNING_SLOT_STATE.slice(i + 1));
        setSigns(symbolsState);

        if (i === SYMBOLS_COUNT - 1) {
          setSlotMachineState(SpinState.STOPPED);
        }
      }, (i + 1) * 500);
    }
  };

  return (
      <div>
          <LoadingSpinner show={initialLoad} />
          {
          failed && initialized ? (
              <div className="mt-2 text-xl text-center font-semibold">
                  <p>You are out of balance, sorry</p>
              </div>
          ) : (
            outOfTheGame && initialized ? (
                <div className="mt-2 text-xl text-center font-semibold">
                    <p>Thank you for the game, <br /> looking forward seeing you soon!</p>
                </div>
            ) : (
                <>
                    <SlotsTable
                        state={slotMachineState}
                        symbols={signs}
                    />
                    <SessionDataUi
                        slotMachineState={slotMachineState}
                        credits={credits}
                        initialized={initialized}
                        cashOut={cashOut}
                        startGame={startGame}
                    />
                </>
            )
          )
        }
      </div>
  );
};
