import { ApiClient } from "../../../core/services/api-client";
import type { ServerResponseInterface } from "../interfaces/infrastructure/server-response.interface";
import type { SessionInitialize } from "../interfaces/infrastructure/server-state.interface";

export const GameSlotsService = new class {
  private base = `/games/slots`;

  public async initiate(): Promise<ServerResponseInterface<SessionInitialize>> {
    const { data } = await ApiClient.post<
        ServerResponseInterface<SessionInitialize>
    >(`${this.base}/init`);

    return data;
  }

  public async roll(): Promise<ServerResponseInterface<SessionInitialize>> {
    const { data } = await ApiClient.post<
        ServerResponseInterface<SessionInitialize>
    >(`${this.base}/roll`);

    return data;
  }

  public async cashOut(): Promise<ServerResponseInterface<SessionInitialize>> {
    const { data } = await ApiClient.post<
        ServerResponseInterface<SessionInitialize>
    >(`${this.base}/cash-out`);

    return data;
  }
}();
