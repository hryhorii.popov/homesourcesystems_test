export interface ServerResponseInterface<T> {
  data: {
    attempt: T[];
  };
}
