export type Sign = "c" | "l" | "o" | "w";

export interface SlotSymbol {
  value: Sign | null;
  state: SpinState;
}

export enum SpinState {
  RUNNING = "running",
  STOPPED = "stopped",
}

export interface SlotsGameProps {
  symbols: SlotSymbol[];
  state: SpinState;
}
