import type { RouteObject } from "react-router";
import React from "react";
import { Navigate } from "react-router-dom";
import { NotFoundPage } from "./pages/not-found.page";
import { FrontPage } from "./pages/front.page";

export enum BaseRouterConfigEnum {
  HOME = "/",
  NTF_REDIRECT = "*",
  NTF = "/404",
}

export const BaseRouterConfig: RouteObject[] = [
  {
    path: BaseRouterConfigEnum.HOME,
    element: <FrontPage />,
  }, {
    path: BaseRouterConfigEnum.NTF,
    element: <NotFoundPage />,
  }, {
    path: BaseRouterConfigEnum.NTF_REDIRECT,
    element: <Navigate to={BaseRouterConfigEnum.NTF} />,
  },
];
