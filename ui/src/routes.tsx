import { createBrowserRouter, type RouteObject } from "react-router-dom";
import { BaseRouterConfig } from "./modules/base/base.router.config";

const routes: RouteObject[] = [
  ...BaseRouterConfig,
];

const Router = createBrowserRouter(routes);

export default Router;
