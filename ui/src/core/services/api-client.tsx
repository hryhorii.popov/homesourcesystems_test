import axios from "axios";

export const ApiClient = axios.create({
  baseURL: "http://localhost:80/api/v1",
});

ApiClient.interceptors.request.use(async (request: any) => {
  const token = localStorage.getItem("game_session_id");

  if (token) {
    request.headers!["Game"] = `${token}`;
  }

  return request;
});
