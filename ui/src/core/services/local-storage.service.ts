export const LocalStorageService = new class {
  public get(key: string) {
    return window.localStorage.getItem(key);
  }

  public set(key: string, value: any) {
    return window.localStorage.setItem(key, value);
  }

  public delete(key: string) {
    window.localStorage.removeItem(key);
  }
}();
