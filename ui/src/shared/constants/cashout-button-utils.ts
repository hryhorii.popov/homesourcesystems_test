export enum Direction {
  Top = "top",
  Left = "left",
  Right = "right",
  Bottom = "bottom",
}

export const ALL_DIRECTIONS = [Direction.Top, Direction.Left, Direction.Right, Direction.Bottom];

export const REVERSE_DIRECTION_MAP = {
  [Direction.Top]: Direction.Bottom,
  [Direction.Bottom]: Direction.Top,
  [Direction.Left]: Direction.Right,
  [Direction.Right]: Direction.Left,
};

export const DIR_ACTION_MAP = {
  [Direction.Top]: { y: -300, x: 0 },
  [Direction.Bottom]: { y: 300, x: 0 },
  [Direction.Left]: { y: 0, x: -300 },
  [Direction.Right]: { y: 0, x: 300 },
};
