export const SYMBOL_STRING_EMOJI_MAP: Record<string, string> = {
  c: "🍒",
  l: "🍋",
  o: "🍊",
  w: "🍉",
};
