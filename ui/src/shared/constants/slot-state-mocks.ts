import { type SlotSymbol, SpinState } from "../../modules/slots/interfaces/slot.interface";

export const DEFAULT_INITIAL_SLOT_STATE: SlotSymbol[] = [
  { value: "c", state: SpinState.STOPPED },
  { value: "l", state: SpinState.STOPPED },
  { value: "o", state: SpinState.STOPPED },
];

export const DEFAULT_RUNNING_SLOT_STATE: SlotSymbol[] = [
  { value: null, state: SpinState.RUNNING },
  { value: null, state: SpinState.RUNNING },
  { value: null, state: SpinState.RUNNING },
];
