import React, { Fragment } from "react";
import { Transition } from "@headlessui/react";
import { classNames } from "../helpers/generic";

interface LoadingSpinnerInterface {
  wrapperClassName?: string;
  spinnerClassName?: string;
  show: boolean;
}
export default function LoadingSpinner({
  wrapperClassName,
  spinnerClassName,
  show,
}: LoadingSpinnerInterface) {
  return (
      <Transition
          show={show}
          as={Fragment}
          enter="transform ease-out duration-300 transition"
          enterFrom="translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
          enterTo="translate-y-0 opacity-100 sm:translate-x-0"
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
      >
          <div className={wrapperClassName ?? "w-100 h-100 fixed text-center bg-white left-0 right-0 top-0 bottom-0 z-50"}>
              <div className="flex flex-auto flex-col justify-center items-center p-4 md:p-5 h-full">
                  <div className="flex justify-center">
                      <div
                          className={classNames(
                            spinnerClassName ?? "",
                            "animate-spin inline-block w-6 h-6 border-[3px] border-current border-t-transparent text-blue-600 rounded-full",
                          )}
                          role="status"
                          aria-label="loading"
                      >
                          <span className="sr-only">Loading...</span>
                      </div>
                  </div>
              </div>
          </div>
      </Transition>
  );
}
